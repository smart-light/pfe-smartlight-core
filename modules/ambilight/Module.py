# -*- coding: utf-8 -*-

from Api import Api
from core.network import result

import json
import os
import time

class Module:

    def __init__(self, groupLamp, savedData):
        self.api = Api(groupLamp)

    def call(self, args):
        color = None

        if 'top' in args:
            color = args['left']

        if not color:
             return False

        R = int(color[0:2], 16)/255.
        G = int(color[2:4], 16)/255.
        B = int(color[4:6], 16)/255.
        Y = 0.3 * R + 0.59 * G + 0.11 * B

        self.api.color(color, on=True, luminosity=Y*70)
        return True
