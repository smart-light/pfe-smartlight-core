# -*- coding: utf-8 -*-

class Module:

    def __init__(self, groupLamp, savedData):
        self.groupLamp = groupLamp

    def getConfig(self):
        pass

    def getConfigStruct(self):
        pass

    def getSavedData(self):
        pass

    def onShutdown(self):
        pass

    def thread(self):
        pass
