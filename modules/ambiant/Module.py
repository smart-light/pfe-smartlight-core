# -*- coding: utf-8 -*-

from Api import Api
from core.network import result

import json
import os
import urllib

class Module:

    def __init__(self, groupLamp, savedData):
        self.api = Api(groupLamp)
        
        if not savedData:
            savedData = {}

        self.savedData = savedData
        self.ambiant = {}

        if 'color' in savedData:
            self.api.color(**savedData)

        path = '%s/config' % os.path.dirname(os.path.abspath(__file__))

        if os.path.isfile(path):
            file = open(path, 'r')
            self.ambiant = json.loads(file.read())
            file.close()

        if not self.ambiant:
            self.resetAmbiantValue()

    def call(self, args):
        color = None
        time = 2000
        luminosity = 70

        if 'time' in args:
            time = int(args['time']) * 1000

        if 'color' in args:
            color = args['color']

        elif 'ambiant' in args:
            ambiant = urllib.unquote( args['ambiant'] ).decode( 'utf8' )

            if ambiant == 'on':
                self.api.on()
            elif ambiant == 'off':
                self.api.off()
            elif ambiant in self.ambiant:
                color = self.ambiant[ambiant]['color']
                luminosity = self.ambiant[ambiant]['luminosity']

                for tmp in self.ambiant:
                    if 'selected' in self.ambiant[tmp]:
                        del self.ambiant[tmp]['selected']

                self.ambiant[ambiant]['selected'] = True
            else:
                return result.error('Ambiance unknow')

        if not color:
             return False

        self.savedData['color'] = color
        self.savedData['luminosity'] = luminosity
        self.api.color(color, time=time, luminosity=luminosity)
        return True

    def getConfig(self):
        return self.ambiant

    def onShutdown(self):
        file = open('%s/config' % os.path.dirname(os.path.abspath(__file__)), 'w+')
        file.write(json.dumps(self.ambiant))
        file.close()

    def resetAmbiantValue(self):
        self.ambiant = {
            'Energie': {"color": "0xff0000", "luminosity": 30},
            'Détente': {"color": "0x0000ff", "luminosity": 30},
            'Travail': {"color": "0x00ffff", "luminosity": 30},
            'Normal': {"color": "0xffffff", "luminosity": 100},
        }
