# -*- coding: utf-8 -*-

from Api import Api

import time

class Module:

    def __init__(self, groupLamp, savedData):
        self.api = Api(groupLamp)

    def thread(self):

        speed = 3000

        while self.threadIsRunning:

            timer = 4*60*60 / speed
            self.api.color('000000', time=timer*1000, luminosity=0)
            # print timer
            time.sleep(timer)

            if not self.threadIsRunning:
                return

            timer = 8*60*60 / speed
            self.api.color('495CFF', time=timer*1000, luminosity=100)
            # print timer
            time.sleep(timer)

            if not self.threadIsRunning:
                return

            timer = 12*60*60 / speed
            self.api.color('FDD131', time=timer*1000, luminosity=100)
            # print timer
            time.sleep(timer)
