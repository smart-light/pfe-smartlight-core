# -*- coding: utf-8 -*-

from Api import Api

import time

class Module:

    def __init__(self, groupLamp, savedData):
        self.api = Api(groupLamp)
        self.api.on()

    def thread(self):
        while self.threadIsRunning:
            self.api.color('FF0000', time=1000, luminosity=100)
            time.sleep(1)

            self.api.color('FF0000', time=1000, luminosity=0)
            time.sleep(1)
