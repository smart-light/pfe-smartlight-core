# -*- coding: utf-8 -*-

from Api import Api

import time

class Module:

    def __init__(self, groupLamp, savedData):
        self.api = Api(groupLamp)
        self.api.on()

    def thread(self):
        while self.threadIsRunning:
            for i in range(3):
                self.api.color( hex( 255 << i*8 )[2:].zfill(6) )
                time.sleep(1)
