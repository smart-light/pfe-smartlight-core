from core.module.Log import Log

from colorpy import colormodels
import json
import httplib

class Api:

    """
    argument des fonctions:
        lampId: id de la lampe
        color: code hexadecimal de la couleur attendu
        time: temps de la transition (500ms par defaut)
        luminosity: pourcentage de la luminosite de la lampe (de 0 a 100)

    """

    def __init__(self, host='192.168.1.2', login = 'SmartLight'):
        self.host = host
        self.login = login

    def color(self, lampId, color = 'FFFFFF', **args):
        return self._set_state(lampId, xy=self._colorToXyz(color), **args)

    def luminosity(self, lampId, luminosity=70, **args):
        return self._set_state(lampId, luminosity=luminosity, **args)

    def on(self, lampId, color = 'FFFFFF', **args):
        if not 'xy' in args and not 'hue' in args:
            args['xy'] = self._colorToXyz(color)
        return self._set_state(lampId, on=True, **args)

    def off(self, lampId, **args):
        return self._set_state(lampId, on=False, **args)

    def _call(self, *args, **kwargs): # path='', method='GET', data=None

        method = kwargs.get('method', 'GET')
        data = kwargs.get('data', None)
        path = kwargs.get('path', '')

        path = '/api/%s%s' % (
            self.login,
            path,
        )

        try:
            conn = httplib.HTTPConnection( self.host, 80)
            conn.request(method, path, json.dumps(data))
        except Exception as e:
            Log.error('Philips API connection error (%s)' % e)
            return False

        response = conn.getresponse()
        result = json.loads(response.read())
        conn.close()

        if isinstance(result, list) and result[0].get('error', None):
            if result[0]['error']['type'] == 1: # not loged
                if self._login():
                    return self._call(*args, **kwargs)

        return result

    def _colorToXyz(self, color):

        color = color.replace('0x', '')

        red = int(color[0:1], 16)
        green = int(color[2:4], 16)
        blue = int(color[4:6], 16)

        redScale = float(red) / 255.0
        greenScale = float(green) / 255.0
        blueScale = float(blue) / 255.0

        colormodels.init(
            phosphor_red=colormodels.xyz_color(0.64843, 0.33086),
            phosphor_green=colormodels.xyz_color(0.4091, 0.518),
            phosphor_blue=colormodels.xyz_color(0.167, 0.04)
        )

        xyz = colormodels.irgb_color(red, green, blue)
        xyz = colormodels.xyz_from_rgb(xyz)
        xyz = colormodels.xyz_normalize(xyz)

        return [xyz[0], xyz[1]]

    def _login(self, retry=0):
        data = {
            'devicetype': 'SmartLight',
            'username': self.login
        }

        conn = httplib.HTTPConnection( self.host, 80)
        conn.request('POST', '/api', json.dumps(data))
        response = conn.getresponse()
        result = json.loads(response.read())
        conn.close()

        return not isinstance(result, list) and result[0].get('error', None)

    def _set_state(self, lampId, **data):

        # transitiontime = int * 100 ms
        if 'time' in data:
            data['transitiontime'] = int(round( data['time'] / 100 ))
            del data['time']
        else:
            data['transitiontime'] = 5 # 500ms par defaut

        if 'color' in data:
            data['xy'] = self._colorToXyz(data['color'])
            del data['color']

        if 'luminosity' in data:
            data['bri'] = int(round(255 * (data['luminosity'] / 100.0)))
            del data['luminosity']

        return self._call(
            path='/lights/%s/state' % lampId,
            method='PUT',
            data=data,
        )
