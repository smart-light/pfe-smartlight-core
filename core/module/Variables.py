import os
import time


class Variables:

    startTime = time.time()

    @classmethod
    def init(cls):

        pathRoot = '%s/../../' % os.path.dirname(os.path.abspath(__file__))

        cls.pathRoot = pathRoot
        cls.pathConfig = '%s/config/' % pathRoot

        if not os.path.exists(pathRoot):
            os.makedirs(pathRoot)

        if not os.path.exists(cls.pathConfig):
            os.makedirs(cls.pathConfig)


# pour resoudre le probleme intermodule
# la classe statique etait instancier plusieur fois
import sys
if not hasattr(sys.modules['__main__'], 'Variables'):
    sys.modules['__main__'].Variables = Variables
else:
    Variables = sys.modules['__main__'].Variables
