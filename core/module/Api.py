class Api:

    def __init__(self, groupLamp, externApi='PhilipsApi', **arg):
        self.groupLamp = groupLamp

        module = __import__('core.externApi.' + externApi,
            globals(),
            locals(),
            ['Api'],
            -1
        )

        self.api = module.Api(**arg)

    def color(self, color, **arg):
        arg['color'] = color
        return self('color', **arg)


    def luminosity(self, luminosity, **arg):
        arg['luminosity'] = luminosity
        return self('luminosity', **arg)

    def on(self, **arg):
        return self('on', **arg)

    def off(self, **arg):
        return self('off',**arg)

    def __call__(self, handle = '', **arg):

        listLamp = None

        if not handle:
            return False

        if self.groupLamp.listLamp:
            listLamp = self.groupLamp.listLamp

        if 'lampId' in arg:
            listLamp = lampId['lampId']

        if not listLamp:
            return True

        if len(listLamp) > 1:
            result = []
            for id in listLamp:
                result.append(self._call(handle, id, **arg))

            return result if len(result) > 0 else False

        return self._call(handle, listLamp, **arg)

    def _call(self, handle, lampId, **arg):
        if hasattr(self.api, handle):
            return getattr(self.api, handle)(lampId, **arg)

        return False
