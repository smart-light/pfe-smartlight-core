from Log import Log
from Variables import Variables

import os
from threading import Thread

class ModuleManager:

    moduleNameList = []
    handleModuleList = {}
    threadList = []

    @classmethod
    def addModule(cls, name):

        if not os.path.isdir(Variables.pathRoot + 'modules/' + name):
            return False

        if not name in cls.moduleNameList:
            cls.moduleNameList.append(name)

        return True

    @classmethod
    def startModule(cls, name, savedData, grouplamp, lastModuleName):

        if not os.path.isdir(Variables.pathRoot + 'modules/' + name):
            return False

        try:

            module = __import__(
                'modules.' + name + '.Module',
                globals(),
                locals(),
                ['Module'],
                -1
            )

            if lastModuleName:
                for thread in cls.threadList:
                    if thread.name == grouplamp.name:
                        cls._killThread(thread)

                cls.callModuleMethod(lastModuleName, 'onShutdown')

            module = module.Module(grouplamp, savedData)
            cls.handleModuleList[ grouplamp.name ] = module

            if hasattr(module, 'thread'):
                def addThread(handle):
                    try:
                        handle()

                    except Exception as e:
                        Log.crash(e)

                cls.handleModuleList[grouplamp.name].threadIsRunning = True
                thread = Thread(name=grouplamp.name, target=addThread, args=(module.thread,))
                cls.threadList.append(thread)
                thread.start()

            return module

        except ImportError as e:
            Log.error('erreur d\'importation du module %s (%s)' % (name, e))
            return False

    @classmethod
    def callModuleMethod(cls, groupLampName, methodName, arg = {}):

        if not cls.handleModuleList.has_key( groupLampName ):
            return False

        module = cls.handleModuleList[ groupLampName ]

        if not hasattr(module, methodName):
            return False

        if arg == None or len(arg) <= 0:
            result = getattr(module, methodName)()
        else:
            result = getattr(module, methodName)(arg)

        if result:
            return result

        return True

    @classmethod
    def shutdown(cls):

        nb = 0

        for thread in cls.threadList:
            cls._killThread(thread)
            nb += 1

        for moduleName in cls.handleModuleList:
            print moduleName
            cls.callModuleMethod(moduleName, 'onShutdown')

        Log.debug( '%d Thread arreter' % nb )

    @classmethod
    def _killThread(cls, thread):
        if not thread:
            return False

        if thread.name in cls.handleModuleList:
            cls.handleModuleList[thread.name].threadIsRunning = False


        thread._Thread__stop()
        return True


# pour resoudre le probleme intermodule
# la classe statique etait instancier plusieur fois
import sys
if not hasattr(sys.modules['__main__'], 'ModuleManager'):
    sys.modules['__main__'].ModuleManager = ModuleManager
else:
    ModuleManager = sys.modules['__main__'].ModuleManager
