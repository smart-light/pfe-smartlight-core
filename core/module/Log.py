from Variables import Variables

import logging


class Log:

    logger = None
    logPath = ''

    @classmethod
    def init(cls):

        cls.logger = logging.getLogger('smartLight')
        hdlr = logging.FileHandler('%s/smartLight.log' % Variables.pathRoot)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)

        cls.logger.addHandler(hdlr)
        cls.logger.setLevel(logging.DEBUG)

    @classmethod
    def debug(cls, text):
        cls.logger.info(text)
        print('Debug: {0}'.format(text))

    @classmethod
    def crash(cls, text):
        cls.logger.critical(text, exc_info=True)

    @classmethod
    def error(cls, text):
        cls.logger.error(text)
        print('Error: {0}'.format(text))


# pour resoudre le probleme intermodule
# la classe statique etait instancier plusieur fois
import sys
if not hasattr(sys.modules['__main__'], 'Log'):
    sys.modules['__main__'].Log = Log
else:
    Log = sys.modules['__main__'].Log
