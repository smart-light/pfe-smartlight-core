from Log import Log
from ModuleManager import ModuleManager
from Variables import Variables

import hashlib
import json
import os
import uuid


class GroupLamp:

    def __init__(self, name='', key='', moduleName='', listLamp=[], savedData=None):

        self.name = name
        self.key = key
        self.listLamp = listLamp
        self.moduleName = ''

        if moduleName:
            self.moduleLoad(moduleName, savedData)

    def addLamp(self, idLamp):
        if not idLamp in self.listLamp:
            self.listLamp.append(idLamp)

    def delLamp(self, idLamp):
        if idLamp in self.listLamp:
            self.listLamp.remove(idLamp)

    def moduleLoad(self, moduleName, savedData=None):

        Log.debug(
            'groupe d\'ampoule "%s" charge le module: %s' % (
                self.name,
                moduleName
            )
        )

        self.module = ModuleManager.startModule(moduleName, savedData, self, self.moduleName)
        self.moduleName = moduleName

    def save(self):

        path = '%s/group-lamp/%s' % (Variables.pathConfig, self.key)
        fichier = open(path, 'w+')
        fichier.write(json.dumps({
            'savedData':  self.module.getSavedData() if self.module and hasattr(self.module, 'getSavedData') else None,
            'moduleName': self.moduleName,
            'listLamp':   self.listLamp,
            'name':       self.name,
            'key':        self.key,
        }))
        fichier.close()


class GroupLampManager:

    groupLampList = {}
    threadList = []

    @classmethod
    def add(cls, create=True, **arg):

        name = 'GroupLamp'

        if 'name' in arg:
            name = arg['name']

        if not 'key' in arg:
            arg['key'] = hashlib.sha512(
                str(arg) + str(uuid.uuid4())[:10]
            ).hexdigest()

        groupLamp = GroupLamp(**arg)
        cls.groupLampList[arg['key']] = groupLamp

        if create:
            groupLamp.save()

    @classmethod
    def getByName(cls, name):
        for key in cls.groupLampList:
            if name == cls.groupLampList[key].name:
                return cls.groupLampList[key]

        return None

    @classmethod
    def load(cls):

        confPath = '%s/group-lamp/' % Variables.pathConfig
        liste = os.listdir(confPath)
        nb = 0

        for fichier in liste:

            if not os.path.exists(confPath + fichier):
                continue

            if fichier[0] == '.':
                continue

            fp = open(confPath + fichier, 'r')
            data = json.loads(fp.read())
            fp.close()

            cls.add(False, **data)
            nb += 1

        Log.debug('%d goupe d\'ampoule chargee' % nb)

    @classmethod
    def shutdown(cls):

        for thread in cls.threadList:
            thread._Thread__stop()

        for key in cls.groupLampList:
            cls.groupLampList[key].save()


# pour resoudre le probleme intermodule
# la classe statique etait instancier plusieur fois
import sys
if not hasattr(sys.modules['__main__'], 'GroupLampManager'):
    sys.modules['__main__'].GroupLampManager = GroupLampManager
else:
    GroupLampManager = sys.modules['__main__'].GroupLampManager
