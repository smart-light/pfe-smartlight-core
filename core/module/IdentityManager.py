from Log import Log
from Variables import Variables
from ..network import result

import hashlib
import json
import os
import time
import uuid


class Identity:

    def __init__(
        self, login='', password='', key='',
        isAdmin=False, token='', salt_token=''
    ):

        self.login = login
        self.password = password
        self.isAdmin = isAdmin
        self.key = key
        self.token = token
        self.salt_token = salt_token

    def save(self):
        path = '%s/identity/%s' % (Variables.pathConfig, self.key)
        fichier = open(path, 'w+')
        fichier.write(json.dumps(vars(self)))
        fichier.close()


class IdentityManager:

    identityList = {}

    @classmethod
    def add(cls, create=True, **arg):

        if create:
            arg['password'] = str(uuid.uuid4())[:20]

        if not 'key' in arg:
            arg['key'] = hashlib.sha512(
                str(arg) + str(uuid.uuid4())[:10]
            ).hexdigest()

        identity = Identity(**arg)
        cls.identityList[arg['key']] = identity

        if create:
            identity.save()

            return arg['password']


    @classmethod
    def createSalt(cls, login):
        identity = cls.getByLogin(login)
        if identity:
            identity.salt_token = str(uuid.uuid4())[:20]
            identity.save()

            return identity.salt_token

        return result.error('User unknown')

    @classmethod
    def getByLogin(cls, login):
        for key in cls.identityList:
            if login == cls.identityList[key].login:
                return cls.identityList[key]

        return None

    @classmethod
    def getByToken(cls, token):
        for key in cls.identityList:
            if token == cls.identityList[key].token:
                return cls.identityList[key]

        return None

    @classmethod
    def hashPassword(cls, identity):
        return hashlib.sha256(
            identity.salt_token + identity.password
        ).hexdigest()


    @classmethod
    def isAdmin(cls, identity):
        return identity.isAdmin if hasattr(identity, 'isAdmin') else False

    @classmethod
    def load(cls):

        confPath = '%s/identity/' % Variables.pathConfig
        liste = os.listdir(confPath)
        nb = 0

        for fichier in liste:

            if not os.path.exists(confPath + fichier):
                continue

            if fichier[0] == '.':
                continue

            fp = open(confPath + fichier, 'r')
            data = json.loads(fp.read())
            fp.close()

            cls.add(False, **data)
            nb += 1

        Log.debug('%d identitee chargee' % nb)

    @classmethod
    def login(cls, login, password):

        identity = cls.getByLogin(login)

        if not identity:
            return result.error('User unknown')

        if password == cls.hashPassword(identity):
            identity.token = str(uuid.uuid4())[:20]
            identity.save()

            return identity.token

        return result.error('Password incorrect')

    @classmethod
    def shutdown(cls):
        for key in cls.identityList:
            cls.identityList[key].save()


# pour resoudre le probleme intermodule
# la classe statique etait instancier plusieur fois
import sys
if not hasattr(sys.modules['__main__'], 'IdentityManager'):
    sys.modules['__main__'].IdentityManager = IdentityManager
else:
    IdentityManager = sys.modules['__main__'].IdentityManager
