# -*- coding: utf-8 -*-

from ..decorators import logged

from .. import result
from core.module.IdentityManager import IdentityManager

from bottle import *
import json


@route('/account')
@logged(admin=True)
def account():
    result = {}
    for key, identity in IdentityManager.identityList.items():
        result[identity.login] = vars(identity)

    return result


@route('/account/add/:login')
@route('/account/add/:login/:admin')
@logged(admin=True)
def addAccount(login='', admin=False):

    if IdentityManager.getByLogin(login):
        return result.error('Account already exist')

    user = {
        'login': login,
    }

    user['isAdmin'] = True if admin=="admin" else False
    
    return IdentityManager.add(True, **user)


@route('/login')
@route('/login/:login')
@route('/login/:login/:hashed_pass')
def login(login='', hashed_pass=''):

    if not login:
        return result.error('not login',)

    if not hashed_pass:
        return IdentityManager.createSalt(login)

    return IdentityManager.login(login, hashed_pass)


@route('/logout')   
@logged(get_token=True)
def logout(token):
    identity = IdentityManager.getByToken(token)
    if identity:
        identity.token = ''
        identity.save()

        return result.success('Correctly logout')

    return result.error('Invalid token')