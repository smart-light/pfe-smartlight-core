# -*- coding: utf-8 -*-

from .. import result
from ..decorators import logged
from core.module.ModuleManager import ModuleManager

from bottle import *
import json

@route('/module')
@logged()
def account():
    return json.dumps(ModuleManager.moduleNameList)


@route('/module/:groupLamp/config')
@post('/module/:groupLamp/config')
@logged()
def account(groupLamp):
    data = dict(request.POST)
    data = ModuleManager.callModuleMethod(groupLamp, 'setConfig' if len(data) > 0 else 'getConfig', data if len(data) > 0 else None)
    return json.dumps(data) if data else result.error('Method not implemented')


@post('/module/:groupLamp/call')
@logged()
def account(groupLamp):
    data = dict(request.POST)
    data = ModuleManager.callModuleMethod(groupLamp, 'call', data)
    return json.dumps(data) if data != False else result.error('Method not implemented')
