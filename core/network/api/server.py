# -*- coding: utf-8 -*-

from ..decorators import logged
from core.module.Variables import Variables

from bottle import *


@route('/info')
@logged()
def account():
    return {
        'uptime': int(time.time() - Variables.startTime),
    }
