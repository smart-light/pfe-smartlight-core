# -*- coding: utf-8 -*-

from ..decorators import logged

from .. import result
from core.module.GroupLampManager import GroupLampManager

from bottle import *
import json

@route('/group-lamp')
@logged()
def groupLampList():
    result = {}
    for key, groupLamp in GroupLampManager.groupLampList.items():
        result[groupLamp.name] = vars(groupLamp)
        if 'module' in result[groupLamp.name]:
            del result[groupLamp.name]['module']

    print result
    return json.dumps(result) 


@route('/group-lamp/add/:groupName')
@logged()
def groupLampAdd(groupName):

    if GroupLampManager.getByName(groupName):
        return result.error('Group already exist')

    groupLamp = {
        'name': groupName,
    }

    return GroupLampManager.add(True, **groupLamp)


@route('/group-lamp/:groupName/add-lamp/:idLamp')
@logged()
def groupLampAddLamp(groupName, idLamp):

    groupLamp = GroupLampManager.getByName(groupName)

    if not groupLamp:
        return result.error('Group not exist')

    groupLamp.addLamp(idLamp)
    return result.success('ok')


@route('/group-lamp/:groupName/del-lamp/:idLamp')
@logged()
def groupLampAddLamp(groupName, idLamp):

    groupLamp = GroupLampManager.getByName(groupName)

    if not groupLamp:
        return result.error('Group not exist')

    groupLamp.delLamp(idLamp)
    return result.success('ok')


@route('/group-lamp/:groupName/list-lamp')
@logged()
def groupLampAddLamp(groupName, idLamp):

    groupLamp = GroupLampManager.getByName(groupName)

    if not groupLamp:
        return result.error('Group not exist')

    return groupLamp.listLamp


@route('/group-lamp/:groupName/module')
@logged()
def groupLampAddLamp(groupName):

    groupLamp = GroupLampManager.getByName(groupName)

    if not groupLamp:
        return result.error('Group not exist')

    return groupLamp.moduleName


@route('/group-lamp/:groupName/module/:moduleName')
@logged()
def groupLampAddLamp(groupName, moduleName):

    groupLamp = GroupLampManager.getByName(groupName)

    if not groupLamp:
        return result.error('Group not exist')

        groupLamp = GroupLampManager.getByName(groupName)

    if groupLamp.moduleName == moduleName:
        return ''

    return groupLamp.moduleLoad(moduleName)
