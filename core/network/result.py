# -*- coding: utf-8 -*-

from bottle import response


def error(msg='', code=400):

    response.status = code

    return {
        'error': True,
        'msg': msg,
    }



def success(msg=''):
    return {
        'sucess': True,
        'msg': msg,
    }