# -*- coding: utf-8 -*-
from core.module.IdentityManager import IdentityManager
import result

from functools import wraps
from bottle import request

def logged(admin=False, get_token=False, **defaults):

    def decorator(func):
    
        @wraps(func)
        def wrapper(*args, **kwargs):
            token = request.get_header('X-SmartLight-Token')
            if token:
                identity = IdentityManager.getByToken(token)
                
                if identity:
                    if admin and not IdentityManager.isAdmin(identity):
                        return result.error('admin required', 401)
                    
                    if get_token:
                        kwargs['token'] = token
                        
                    return func(*args, **kwargs)

            return result.error('login required', 401)
    
        return wrapper
    
    return decorator