# -*- coding: utf-8 -*-
from bottle import *

from api.account import *
from api.groupLamp import *
from api.module import *
from api.server import *


class NetworkServer:

    @route('/')
    def home():
        return 'SmartLight'

    def start(self, port):
        debug()

        # ne pas metre "reloader=True" => duplicatio de de possessus
        run(host='0.0.0.0', port=port, reloader=False)
