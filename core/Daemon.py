#-*- coding: utf-8 -*-

from module.GroupLampManager import GroupLampManager
from module.IdentityManager import IdentityManager
from module.Log import Log
from module.ModuleManager import ModuleManager
from module.Variables import Variables
from network.NetworkServer import NetworkServer

import os
import sys


class Daemon:

    def __init__(self):

        # permet l'importation depuis les module
        sys.path[:0] = ['{0}/module'.format(
            os.path.dirname(os.path.abspath(__file__))
        )]

        Variables.init()
        Log.init()

    def loadModules(self):

        liste = os.listdir(Variables.pathRoot + 'modules')
        nb = 0

        for repertoire in liste:
            if ModuleManager.addModule(repertoire):
                nb += 1

        Log.debug('%d modules loaded' % nb)

    def start(self):

        try:
            IdentityManager.load()
            self.loadModules()
            GroupLampManager.load()

            NetworkServer().start(8000)

        except Exception as e:
            Log.crash(e)

        finally:
            ModuleManager.shutdown()
            GroupLampManager.shutdown()
            IdentityManager.shutdown()
